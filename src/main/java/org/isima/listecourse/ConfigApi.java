package org.isima.listecourse;

import org.isima.listecourse.Controller.FileController;
import org.isima.listecourse.services.ListeService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



/**
 * @author mariviere4
 * Classe de configuration de l'application
 * 
 */

@Configuration
public class ConfigApi {

    /*Création du file controllere*/
    @Bean
    public FileController fileController()
    {
        return new FileController();
    }
    
    /*Création du file controllere*/
    @Bean
    public ListeService listeService()
    {
        return new ListeService();
    }

}
