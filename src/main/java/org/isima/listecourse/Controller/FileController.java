package org.isima.listecourse.Controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;

/**
 * @author mariviere4
 * Classe contrôleur pour la lecture des fichiers JSON
 */

public class FileController
{
	/*Gets elements du fichier passé en paramètre
	 * file: nom du fichier à lire 
	 * classe : classe des objets à créer à partir du fichier passé en paramètre 
	 * */
    @SuppressWarnings("unchecked")
    public  <T> ArrayList<T> getElementsFromFile(String file, Class<T>  classe)
    {
    	 ArrayList<T> dataObject = new ArrayList<>();
        //Parse du JSON
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader(file))
        {

            //Lecture du fichier
            Object obj = (Object) jsonParser.parse(reader);

            JSONArray data = (JSONArray) obj;            
           
            //Parcours du tableau d'objets JSON
            data.forEach( e -> dataObject.add(parseObject( (String) e , classe )));

        } catch (FileNotFoundException e) {
        	//Catch des problèmes d'ouverture de fichiers
            e.printStackTrace();
        } catch (IOException e) {
        	//Catch des problèmes de lecture
            e.printStackTrace();
        } catch (ParseException e) {
        	// Catch des problèmes de parse
            e.printStackTrace();
        }
        return dataObject;
    }

    /*Parse un objzet JSOn en objet de la classe passée en paramètre
	 * e: objet JSON
	 * classe : classe de l'objet à créer 
	 * */
    private  <T> T parseObject(String e, Class<T> classe)
    {
    	
    	T r  = null;
        Gson gson = new Gson();
        r=gson.fromJson(e , classe  );
        return r;
    }
}