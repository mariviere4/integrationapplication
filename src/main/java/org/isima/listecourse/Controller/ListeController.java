package org.isima.listecourse.Controller;

import org.isima.listecourse.Entity.Liste;
import org.isima.listecourse.services.ListeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ListeController {
	
	@Autowired
	private ListeService listeService;

	  // Getters et setters 
    public ListeService getListeService() {
		return listeService;
	}

	
	public void setListeService(ListeService ls) {
		listeService = ls;
	}
	
	public Liste listecourses = new Liste();
	//Récupérer la liste par défaut
	@GetMapping(value = "/liste")
	public ResponseEntity<Liste> getListe()
	{
		if(listecourses.listeCourse.isEmpty())
		{
			listecourses.listeCourse = listeService.getCodeBarres();
		}
		return ResponseEntity.ok(listecourses);
	}
	//Ajouter le code barre d'un nouveau produit
	@PostMapping(path= "/new")
	public ResponseEntity<Liste> addCode(@RequestBody String code)
	{
		try {
			listecourses.ajouterProduit(code);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok(listecourses);	
	}
	
	//Supprimer un code barre dans la liste
	@PostMapping(path= "/delete")
	public ResponseEntity<Liste> deleteCode(@RequestBody String code)
	{
		try {
			if(listecourses.listeCourse.contains(code))
			{
				listecourses.supprimerProduit(code);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok(listecourses);	
	}
	//Vider la liste
	@PostMapping(path= "/raz")
	public ResponseEntity<Liste> razCode()
	{
		try {
			if(!listecourses.listeCourse.isEmpty())
			{
				listecourses.listeCourse.removeAll(listecourses.listeCourse);	
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok(listecourses);	
	}
	
	
}
