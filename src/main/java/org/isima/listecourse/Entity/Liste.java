package org.isima.listecourse.Entity;

import java.util.ArrayList;
import java.util.List;


/*Classe liste de course*/
public class Liste {
	
	public List<String> listeCourse = new ArrayList<String>();
	
	public void ajouterProduit(String p) throws Exception{
		//listeCourse = new ArrayList();
		if(!listeCourse.contains(p))
		{
			listeCourse.add(p);
		}
	}
	
	public void supprimerProduit(String p) throws Exception{
		//listeCourse = new ArrayList();
		if(listeCourse.contains(p))
		{
			listeCourse.remove(p);
		}
	}
	
	//Imprimer la liste de course
	public void imprimerListeCourse() {
		if(!listeCourse.isEmpty())
		{

			listeCourse.stream().forEach(System.out::println);
		}
	}
	
	//Imprimer la totalite de produits dans la liste de course
	public void imprimerTotal() {
		long nbProduit = listeCourse.stream().count();
		System.out.println("Nombre de produits : " + nbProduit);
	}
	

}
