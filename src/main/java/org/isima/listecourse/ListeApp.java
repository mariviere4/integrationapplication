package org.isima.listecourse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/*
 * Point d'entrée de l'application
 * 
 * 
 */
@SpringBootApplication
public class ListeApp {
    public static void main(String[] args) {
        SpringApplication.run(ListeApp.class, args);
    }
}