package org.isima.listecourse.services;

import java.util.List;

import org.isima.listecourse.ConfigApi;
import org.isima.listecourse.Controller.FileController;
import org.springframework.beans.factory.annotation.Autowired;

/*Classe de service permettant l'accès à l'api de liste*/
public class ListeService {
    @Autowired
    ConfigApi configApi;
    
    //Contrôleur pour la lecture des fichiers JSON
    @Autowired
    private FileController fileController;
   

	public void setFileController(FileController fileController) {
		this.fileController = fileController;
	}
	
	
    /*Récupère les informations du produit dont le code est passé en paramètre*/
    public List<String> getCodeBarres() {
        List<String> codebarres = fileController.getElementsFromFile( getClass().getClassLoader().getResource("codeBarre.json").getFile(), String.class);           
    	return codebarres;
    }

}
