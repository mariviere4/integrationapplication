package org.isima.listecourse.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.isima.listecourse.Controller.FileController;
import org.junit.Test;

public class FileControllerTest {
    //Vérifier la récupération des éléments dans le fichier codebarre.json
	@Test
	public void getElementsFromFile() {
		FileController fc = new FileController();
		ArrayList<String> rules = fc.getElementsFromFile(getClass().getClassLoader().getResource("codeBarre.json").getFile(), String.class);
		assertEquals(6,rules.size());
	}

}
