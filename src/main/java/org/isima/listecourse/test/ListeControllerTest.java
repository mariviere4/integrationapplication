package org.isima.listecourse.test;

import static org.junit.Assert.assertTrue;
import org.isima.listecourse.ConfigApi;
import org.isima.listecourse.Controller.FileController;
import org.isima.listecourse.Controller.ListeController;
import org.isima.listecourse.services.ListeService;
import org.junit.Test;

public class ListeControllerTest {
    //Chargement de liste de courses avec les codes barre des produits
	@Test
	public void getListe() {
		ListeController listec = new ListeController();
		ConfigApi ca = new ConfigApi();
		ListeService ls = ca.listeService();
		FileController fc = ca.fileController();
		ls.setFileController(fc);
		listec.setListeService(ls);
		listec.getListe();
		listec.listecourses.imprimerListeCourse();
        assertTrue(listec.listecourses != null);
   
	}
	//Ajouter un produit dans la liste en fonction de son code barre
	@Test
	public void addCode() {
		ListeController listec = new ListeController();
		ConfigApi ca = new ConfigApi();
		ListeService ls = ca.listeService();
		listec.setListeService(ls);
		listec.addCode("356470057709702");
		listec.getListe();		
		listec.listecourses.imprimerListeCourse();
        assertTrue(listec.listecourses != null);
   
	}
	
	//Enlever un produit dans la liste en fonction de son code barre
	@Test
	public void deleteCode() {
		ListeController listec = new ListeController();
		ConfigApi ca = new ConfigApi();
		ListeService ls = ca.listeService();
		listec.setListeService(ls);
		listec.deleteCode("356470057709702");
		listec.listecourses.imprimerListeCourse();
        assertTrue(listec.listecourses != null);
   
	}
	//Vider la liste
	@Test
	public void razCode() {
		ListeController listec = new ListeController();
		ConfigApi ca = new ConfigApi();ListeService ls = ca.listeService();
		listec.setListeService(ls);
		listec.razCode();	
		listec.listecourses.imprimerListeCourse();
        assertTrue(listec.listecourses != null);
   
	}
}
